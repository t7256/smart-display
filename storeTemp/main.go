package main

import (
	"fmt"
	"storeTemp/temperature"
)

func main() {
	fmt.Println("Starting...")
	temperature.Init()
}
