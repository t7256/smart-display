package database

import (
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"
)

const (
	host   = "localhost"
	port   = 5432
	user   = "postgres"
	dbname = "office_conditions"
)
var err error
var db *sql.DB

func ConnectDb() {
	connStr := fmt.Sprintf("user=%s dbname=%s host=%s sslmode=disable port=%d", user, dbname, host, port)

	db, err = sql.Open("postgres", connStr)
	if err != nil {
		panic(err)
	}

	fmt.Printf("\nSuccessfully connected to database!\n")
}

func InsertTempData(temp int, date string, time string, room string) {
	ConnectDb()
	sqlStatement := `INSERT INTO public.temperature (temperature, date, time, room)
	VALUES($1, $2, $3, $4)`
	_, err = db.Exec(sqlStatement, temp, date, time, room)

	if err != nil {
		panic(err)
	}else {
		fmt.Print("\nRow inserted succesfully!\n")
	}
}




