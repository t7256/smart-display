package temperature

import (
	"encoding/json"
	"fmt"
	"os"
	"os/signal"
	"strings"
	"syscall"

	"storeTemp/database"
	mqtt "github.com/eclipse/paho.mqtt.golang"
)

type Response struct {
	Temp int    `json:"temp"`
	Date string `json:"date"`
}

var messagePubHandler mqtt.MessageHandler = func(client mqtt.Client, msg mqtt.Message) {
	fmt.Printf("\nMessage %s received on topic %s\n", msg.Payload(), msg.Topic())
	//{'temp': 19, 'date': datetime.datetime(2022, 2, 3, 10, 46, 55, 700444)}
	var result Response // initiera variabel (nil)
	err := json.Unmarshal(msg.Payload(), &result)
	if err != nil {
		fmt.Println("Could not parse")
	}
	temp := result.Temp
	split_date := strings.Split(result.Date, " ")
	date := split_date[0]
	time := strings.Split(split_date[1], ".")[0]
	room := strings.Split(msg.Topic(), "/")[0]
	database.InsertTempData(temp, date, time, room)
}

var connectHandler mqtt.OnConnectHandler = func(client mqtt.Client) {
	fmt.Println("Connected to broker")
}

var connectionLostHandler mqtt.ConnectionLostHandler = func(client mqtt.Client, err error) {
	fmt.Printf("Connection Lost: %s\n", err.Error())
}

func Init() {
	keepAlive := make(chan os.Signal)
	signal.Notify(keepAlive, os.Interrupt, syscall.SIGTERM)

	var broker = fmt.Sprintf("%s:1883", os.Args[1])
	options := mqtt.NewClientOptions()
	options.AddBroker(broker)
	options.SetClientID("go_mqtt_example")
	options.SetDefaultPublishHandler(messagePubHandler)
	options.OnConnect = connectHandler
	options.OnConnectionLost = connectionLostHandler

	client := mqtt.NewClient(options)
	token := client.Connect()
	if token.Wait() && token.Error() != nil {
		panic(token.Error())
	}

	topic := "lassemajas/temperature"
	token = client.Subscribe(topic, 1, nil)
	token.Wait()
	fmt.Printf("Subscribed to topic %s\n", topic)
	<-keepAlive
}
